﻿USE compranp;

-- CONSULTAS DE DATOS ANEXADOS

-- COLOCAR EN UNA NUEVA TABLA EL NOMBRE DE LOS PRODUCTOS VENDIDOS

-- 1 paso 
-- creo la tabla
CREATE TABLE productosVendidos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

-- 2 paso 
-- introducir los productos vendidos en la tabla
INSERT INTO productosVendidos (nombre)
  SELECT 
      DISTINCT p.nombre 
    FROM productos p 
      JOIN compran c ON p.codPro = c.codPro;

-- compronar si han entrado los productos
SELECT * FROM productosVendidos v;


-- si han entrado mal y quieres borrar la tabla

-- elimina todos los registros de la tabla
-- pero no reinicia los indices
DELETE FROM productosVendidos;

-- elimina todos los registros de la tabla pero reiniciando los indices
TRUNCATE productosVendidos;

-- CONSULTAS DE CREACION DE TABLA

-- COLOCAR EN UNA NUEVA TABLA EL NOMBRE DE LOS PRODUCTOS VENDIDOS

CREATE TABLE productosVendidos AS 
  SELECT 
      DISTINCT p.nombre 
    FROM productos p 
      JOIN compran c ON p.codPro = c.codPro;

-- comprobar que la consulta anterior ha funcionado correctamente
SELECT * FROM productosVendidos v;

-- COLOCAR EN UNA NUEVA TABLA EL NOMBRE DE LOS PRODUCTOS VENDIDOS
-- si yo quiero crear el campo id

CREATE TABLE productosVendidos (
    id int AUTO_INCREMENT,
    nombre varchar(50),
    PRIMARY KEY(id)
  ) AS 
  SELECT 
      DISTINCT p.nombre 
    FROM productos p 
      JOIN compran c ON p.codPro = c.codPro;

-- COMPROBAR EL RESULTADO

SELECT * FROM productosVendidos v;


-- CONSULTA
-- CREEIS UNA TABLA CON LOS NOMBRES DE LOS PRODUCTOS QUE NO SE HAN VENDIDO
-- PARA REALIZARLO QUIERO QUE UTILICEIS CREATE TABLE AS

-- CONSULTA DE SELECCION

SELECT 
    p.nombre 
    FROM 
      productos p LEFT JOIN compran c USING(codPro)
    WHERE 
      c.codPro IS NULL;

-- INTRODUZCO UN PRODUCTO DE PRUEBAS EN LA TABLA PRODUCTOS
-- ESTE PRODUCTO NO SE HA VENDIDO
INSERT INTO productos (codPro, nombre, precio, numeroClientes)
  VALUES (2000, 'NUEVO', 100, 0);

-- CREO LA CONSULTA DE ACCION
DROP TABLE IF EXISTS productosnovendidos;
CREATE TABLE productosNoVendidos (
  id int AUTO_INCREMENT,
  nombre varchar(200),
  PRIMARY KEY (id)
)
  AS 
    SELECT 
      p.nombre 
      FROM 
        productos p LEFT JOIN compran c USING(codPro)
      WHERE 
        c.codPro IS NULL;

-- comprobar que la tabla esta creada y tiene los productos
SELECT * FROM productosNoVendidos nv;


