﻿USE compranp;

-- CONSULTA 01
-- AÑADIR UN CAMPO A LA TABLA PRODUCTOS DENOMINADO numeroUnidades
-- QUE ME INDIQUE EL NUMERO DE UNIDADES QUE SE HA COMPRADO DE ESE PRODUCTO

-- añadir el campo
ALTER TABLE productos ADD COLUMN numeroUnidades int DEFAULT 0;

-- consulta de seleccion 
-- c1
SELECT 
    c.codPro,
    SUM(c1.cantidad) numero
  FROM compran c join compranp c1  USING (idCompran)
  GROUP BY c.codPro;

-- consulta de actualizacion
UPDATE
    productos p
  JOIN 
    (
      SELECT 
          c.codPro,
          SUM(c1.cantidad) numero
        FROM compran c join compranp c1  USING (idCompran)
        GROUP BY c.codPro
    )c1 USING (codPro)
  SET 
    p.numeroUnidades=c1.numero;
     


    SELECT 
      IF(p.precio>10,"CARO","BARATO") campo, p.precio, p.nombre 
      FROM productos p;

  -- CONSULTA 02
  -- CALCULAR EL PRECIO TOTAL DE LOS PRODUCTOS EN LA TABLA COMPRANP 
  -- TENIENDO EN CUENTA LO SIGUIENTE:
  -- PARA CALCULAR EL PRECIO TOTAL ES MULTIPLICAR EL PRECIO DEL 
  -- PRODUCTO POR LAS UNIDADES

  -- AL NO TENER QUE REALIZAR TOTALES PUEDO REALIZARLA SIN UTILIZAR UNA SUBCONSULTA

  -- CONSULTA DE SELECCION
  SELECT 
      c.cantidad*p1.precio totalCalculado 
    FROM
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro;

  -- CONSULTA DE ACTUALIZACION

    UPDATE 
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro
  
      SET c.total=c.cantidad*p1.precio;
      

  -- COMPROBAR EL RESULTADO
  SELECT * FROM compranp c;



  

  -- CONSULTA 03
  -- CALCULAR EL PRECIO TOTAL DE LOS PRODUCTOS EN LA TABLA COMPRANP 
  -- TENIENDO EN CUENTA LO SIGUIENTE:
  -- SI EL NUMERO DE UNIDADES PEDIDO ES MENOR QUE 10 NO HAY DESCUENTO
  -- SI EL NUMERO DE UNIDADES ESTA ENTRE 10 Y 30 EL DESCUENTO ES DEL 10%
  -- SI EL NUMERO DE UNIDADES ES MAYOR QUE 30 EL DESCUENTO ES DEL 20%
  -- PARA CALCULAR EL PRECIO TOTAL ES MULTIPLICAR EL PRECIO DEL PRODUCTO POR LAS UNIDADES

  -- APROVECHO LA CONSULTA ANTERIOR
    
  -- CONSULTA PARA CUANDO CANTIDAD<10  
    UPDATE 
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro
      SET c.total=c.cantidad*p1.precio
      WHERE c.cantidad<10;

    -- CONSULTA PARA CUANDO LA CANTIDAD ESTA ENTRE 10 Y 30
    UPDATE 
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro
      SET c.total=c.cantidad*p1.precio*0.9
      WHERE c.cantidad BETWEEN 10 AND 30;

    -- CONSULTA PARA CUANDO LA CANTIDAD ES MAYOR QUE 30
    UPDATE 
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro
      SET c.total=c.cantidad*p1.precio*0.8
      WHERE c.cantidad>30;

    -- COMPROBAR QUE TODO SE HA CALCULADO CORRECTAMENTE
    SELECT * FROM compranp c;


    -- QUIERO UTILIZAR LA FUNCION IF PARA PODER REALIZAR TODO EN UNA
    -- SOLA CONSULTA
    
    -- COMPROBAR COMO FUNCIONA IF
    
    SELECT 
        IF(p.precio>10,"CARO","BARATO") campoCalculado,
        p.* 
      FROM productos p;  


  -- INTENTAR APROVECHAR LA FUNCION IF EN LAS CONSULTAS ANTERIORES
  UPDATE 
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro
      SET c.total=c.cantidad*p1.precio*IF(c.cantidad<10,1,IF(c.cantidad<=30,0.9,0.8));


 -- COMPROBAR QUE FUNCIONA
 SELECT * FROM compranp c;


