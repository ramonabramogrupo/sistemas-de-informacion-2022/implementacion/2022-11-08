﻿USE compranp;

-- CONSULTAS DE ELIMINACION

-- EJEMPLO 1
-- ELIMINAR LOS PRODUCTOS CUYO PRECIO ES MAYOR O IGUAL QUE 20 EUROS


-- ELIMINAR TODOS LOS PRODUCTOS CUYO PRECIO SEA MAYOR O IGUAL A 20

-- CONSULTA DE SELECCION 
 SELECT 
    * 
  FROM productos p 
  WHERE p.precio>=20;


-- CONSULTA DE ELIMINACION
  DELETE
  FROM productos  
  WHERE precio>=20;

-- COMPROBAR SI SE HAN BORRADO LOS REGISTROS
SELECT * FROM productos p;
SELECT * FROM compran c;
SELECT * FROM compranp c;



-- OPCION 2
-- OTRA OPCION PARA UTILIZAR DELETE
-- ESTA OPCION ES MEJOR QUE LA DEJEMOS PARA CUANDO TENGAMOS MAS DE 1 TABLA
-- EN LA CONSULTA DE ELIMINACION
  DELETE
    productos.* -- LE ESTOY INDICANDO DE QUE TABLA QUIERO ELIMINAR LOS REGISTROS
    FROM productos 
    WHERE precio>=20;

  -- OPCION 3
  -- OTRA OPCION PARA UTILIZAR DELETE
  -- ESTA OPCION ES MEJOR QUE LA DEJEMOS PARA CUANDO TENGAMOS MAS DE 1 TABLA
  -- EN LA CONSULTA DE ELIMINACION
  DELETE
    productos
    FROM productos 
    WHERE precio>=20;

  -- OPCION 4
  -- OTRA OPCION PARA UTILIZAR DELETE
  -- ESTA OPCION ES MEJOR QUE LA DEJEMOS PARA CUANDO TENGAMOS MAS DE 1 TABLA
  -- EN LA CONSULTA DE ELIMINACION

   DELETE
    p.*
    FROM productos p 
    WHERE p.precio>=20;

-- EJEMPLO 2
-- Eliminar las compras de los productos cuyo precio es mayor o igual 20

-- consulta de seleccion

  SELECT 
      * 
    FROM
      compran c JOIN productos p ON c.codPro = p.codPro
    WHERE p.precio>=20; 

  

-- CONSULTA DE ELIMINACION

-- opcion 1    
    DELETE 
      c.* 
    FROM
      compran c JOIN productos p ON c.codPro = p.codPro
    WHERE p.precio>=20; 

-- opcion 2    
    DELETE 
      c 
    FROM
      compran c JOIN productos p ON c.codPro = p.codPro
    WHERE p.precio>=20; 

  -- OPCION 3
  DELETE compran.*
    FROM productos JOIN compran USING(codPro)
    WHERE precio>=20;

    
  
 -- OPCION CON SUBCONSULTAS
 
 -- SUBCONSULTA EN WHERE
 DELETE FROM compran 
  WHERE codPro IN (SELECT p.codPro FROM productos p WHERE p.precio>=20);

 -- SUBCONSULTA EN EL FROM    
 DELETE c.* FROM compran c JOIN (SELECT p.codPro FROM productos p WHERE p.precio>=20) c1
  USING(codPro);


-- EJEMPLO 3
-- ELIMINAR LAS COMPRAS DE LOS CLIENTES DE SANTANDER

-- CONSULTA DE SELECCION

SELECT 
    * 
  FROM 
      poblacion p 
    JOIN clientes c ON p.idPob = c.poblacion
    JOIN compran c1 ON c.codCli = c1.codCli
  WHERE p.nombre="Santander";


-- CONSULTA DE ELIMINACION
 
-- opcion 1
DELETE 
    c1 
  FROM 
      poblacion p 
    JOIN clientes c ON p.idPob = c.poblacion
    JOIN compran c1 ON c.codCli = c1.codCli
  WHERE p.nombre="Santander";

-- opcion 2
DELETE 
    c1.* 
  FROM 
      poblacion p 
    JOIN clientes c ON p.idPob = c.poblacion
    JOIN compran c1 ON c.codCli = c1.codCli
  WHERE p.nombre="Santander";

 